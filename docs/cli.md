## cli
Command line interface for the WebDAV Bulk Transfer tool

**Example**  
```js
Download command
Usage: download <sourceURL> <localdirectory> <directoriesAndFiles>

Options:
*    --username <value>    The username used to authenticate through the source.
*    --password <value>    The password used to authenticate through the source.
*    --quiet               Disable the verbose mode. Default: false
*    --concurrency <value> The number of calls that can be handled at one time by the tool. Default: 1
*    --recursive           Will transfer directories and files recursively (look in child directories if any). Default: true
*    --keeplocaldirectory  Will keep the local directory before performing the download action. This is useful when you already have some files and want to download the remaining files from the instance. Default: false

The following examples will download directories and files recursively from the given server to the given local directory (10 concurrent connections will be opened each time)
// with node command
node bin/cli.js download "https://server.url" "path/to/local/directory" "/path/to/remote/directory,/path/to/remote/file.ext" --username "username" --password "password" --recursive --concurrency 10
// after a "npm link"
davtransfer download "https://server.url" "path/to/local/directory" "/path/to/remote/directory,/path/to/remote/file.ext" --username "username" --password "password" --recursive --concurrency 10
```
**Example**  
```js
Upload command
Usage: upload <localdirectory> <targetURL>

Options:
*    --username <value>    The username used to authenticate through the source.
*    --password <value>    The password used to authenticate through the source.
*    --certificate <value> The path to the source certificate file.
*    --passphrase <value>  The passphrase to use with the given source certificate.
*    --quiet               Disable the verbose mode. Default: false
*    --cleanafter          Clean the local directory after the execution. Default: false
*    --concurrency <value> The number of calls that can be handled at one time by the tool. Default: 1
*    --recursive           Will transfer directories and files recursively (look in child directories if any). Default: true

The following examples will upload files in the local directory recursively to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
// with node command
node bin/cli.js upload "path/to/local/directory" "https://server.url" --username "username" --password "password" --recursive --concurrency 10 --cleanafter
// after a "npm link"
davtransfer upload "path/to/local/directory" "https://server.url" --username "username" --password "password" --recursive --concurrency 10 --cleanafter
```
**Example**  
```js
Transfer command
Usage: transfer <sourceURL> <targetURL> <directoriesAndFiles>

Options:
*    --localdirectory <value>    The local directory where the downloaded files will be stored before uploading them. Default: "downloaded_files"
*    --sourceusername <value>    The username used to authenticate through the source.
*    --sourcepassword <value>    The password used to authenticate through the source.
*    --sourcecertificate <value> The path to the source certificate file.
*    --sourcepassphrase <value>  The passphrase to use with the given source certificate.
*    --targetusername <value>    The username used to authenticate through the target.
*    --targetpassword <value>    The password used to authenticate through the target.
*    --targetcertificate <value> The path to the target certificate file.
*    --targetpassphrase <value>  The passphrase to use with the given target certificate.
*    --concurrency <value>       The number of calls that can be handled at one time by the tool. Default: 1
*    --quiet                     Disable the verbose mode. Default: false
*    --cleanafter                Clean the local temp directory after the execution. Default: false
*    --recursive                 Will transfer directories and files recursively (look in child directories if any). Default: true
*    --onlynewfiles              Will only transfer files from the source that does not exists in the target. Default: false
*    --keeplocaldirectory        Will keep the local directory before performing the transfer action. This is useful when you already have some files to transfer locally, and only want to download the remaining files before uploading everything. Default: false

The following examples will transfer files recursively from the given source server to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
// with node command
node bin/cli.js transfer "https://source.server" "https://targer.server" --sourceusername "username" --sourcepassword "password" --targetusername "username" --targerpassword "password" --recursive --concurrency 10 --cleanafter
// after a "npm link"
davtransfer transfer "https://source.server" "https://targer.server" --sourceusername "username" --sourcepassword "password" --targetusername "username" --targerpassword "password" --recursive --concurrency 10 --cleanafter
```

* [cli](#markdown-header-cli)
    * [~packageDetails](#markdown-header-clipackagedetails-object) : Object
    * [~program](#markdown-header-cliprogram-node_modulescommander) : node_modules/commander
    * [~log](#markdown-header-clilog-liblog) : lib/log
    * [~main](#markdown-header-climain-srcmain) : src/main

### cli~packageDetails : Object
**Kind**: inner constant of [cli](#markdown-header-cli)  
### cli~program : node_modules/commander
**Kind**: inner constant of [cli](#markdown-header-cli)  
### cli~log : lib/log
**Kind**: inner constant of [cli](#markdown-header-cli)  
### cli~main : src/main
**Kind**: inner constant of [cli](#markdown-header-cli)  
