## main
Main transfer module


* [main](#markdown-header-main)
    * [download(source, localDirectory, filesPattern, params)](#markdown-header-downloadsource-localdirectory-filespattern-params-promise) ⇒ Promise ⏏
    * [upload(localDirectory, target, params)](#markdown-header-uploadlocaldirectory-target-params-promise) ⇒ Promise ⏏
    * [transfer(source, target, filesPattern, params)](#markdown-header-transfersource-target-filespattern-params-promise) ⇒ Promise ⏏

### download(source, localDirectory, filesPattern, params) ⇒ Promise ⏏
This method will download all directories and files in the given {filesPattern} locally from the source instance

**Kind**: Exported function  
**Fulfil**: undefined - Returns undefined when everything goes well  
**Reject**: Error - The error that occured  

| Param | Type | Description |
| --- | --- | --- |
| source | String | The source WebDAV URL |
| localDirectory | string | The local directory where to download files |
| filesPattern | String ⎮ Array | The directories or files to transfer |
| params | Object |  |

### upload(localDirectory, target, params) ⇒ Promise ⏏
This method will download all directories and files in the given {filesPattern} locally from the source instance
Then upload all those directories files to the target instance in the exact same structure.

**Kind**: Exported function  
**Fulfil**: undefined - Returns undefined when everything goes well  
**Reject**: Error - The error that occured  

| Param | Type | Description |
| --- | --- | --- |
| localDirectory | String | The local directory where the downloaded files will be stored before uploading them |
| target | String | The target WebDAV URL |
| params | Object |  |

### transfer(source, target, filesPattern, params) ⇒ Promise ⏏
This method will download all directories and files in the given {filesPattern} locally from the source instance
Then upload all those directories files to the target instance in the exact same structure.

**Kind**: Exported function  
**Fulfil**: undefined - Returns undefined when everything goes well  
**Reject**: Error - The error that occured  

| Param | Type | Description |
| --- | --- | --- |
| source | String | The source WebDAV URL |
| target | String | The target WebDAV URL |
| filesPattern | String ⎮ Array | The directories or files to transfer |
| params | Object |  |

