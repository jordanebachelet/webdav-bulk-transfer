## Modules
Module | Description
------ | -----------
[helper](#markdown-header-helper) | Helper toolbox
[log](#markdown-header-log) | Custom logger
[options](#markdown-header-options) | Options parser
[webdav](#markdown-header-webdav) | WebDAV helper

## helper
Helper toolbox


* [helper](#markdown-header-helper)
    * [cleanDirectory(directory)](#markdown-header-cleandirectorydirectory-promise) ⇒ Promise ⏏
        * [~del](#markdown-header-cleandirectorydel-node_modulesdel) : node_modules/del
        * [~fs](#markdown-header-cleandirectoryfs-node_modulesfs) : node_modules/fs
        * [~path](#markdown-header-cleandirectorypath-node_modulespath) : node_modules/path
        * [~log](#markdown-header-cleandirectorylog-liblog) : lib/log
        * [~getFilesRecursively(directory, recursively)](#markdown-header-cleandirectorygetfilesrecursivelydirectory-recursively-array) ⇒ Array

### cleanDirectory(directory) ⇒ Promise ⏏
Remove the given {directory} and all its content

**Kind**: Exported function  
**Fulfil**: undefined - Returns undefined when everything goes well  
**Reject**: Error - The error that occured  

| Param | Type |
| --- | --- |
| directory | String | 

#### cleanDirectory~del : node_modules/del
**Kind**: inner constant of cleanDirectory  
#### cleanDirectory~fs : node_modules/fs
**Kind**: inner constant of cleanDirectory  
#### cleanDirectory~path : node_modules/path
**Kind**: inner constant of cleanDirectory  
#### cleanDirectory~log : lib/log
**Kind**: inner constant of cleanDirectory  
#### cleanDirectory~getFilesRecursively(directory, recursively) ⇒ Array
Returns the list of files in the given {directoriesAndFiles} list recursively, or not

**Kind**: inner method of cleanDirectory  

| Param | Type |
| --- | --- |
| directory | Array | 
| recursively | Boolean | 

## log
Custom logger


* [log](#markdown-header-log)
    * [isQuiet(checkOnlyRunningMode)](#markdown-header-isquietcheckonlyrunningmode-boolean) ⇒ Boolean ⏏
    * [info()](#markdown-header-info) ⏏
    * [success()](#markdown-header-success) ⏏
    * [warn()](#markdown-header-warn) ⏏
    * [error()](#markdown-header-error) ⏏

### isQuiet(checkOnlyRunningMode) ⇒ Boolean ⏏
Returns true if the tool has to be quiet (meaning not logging everything)

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| checkOnlyRunningMode | Boolean | only checks the running mode (not the environment variables) |

### info() ⏏
Log the given message as a info message

**Kind**: Exported function  
### success() ⏏
Log the given message as a success message

**Kind**: Exported function  
### warn() ⏏
Log the given message as a warn message

**Kind**: Exported function  
### error() ⏏
Log the given message as a error message

**Kind**: Exported function  
## options
Options parser


* [options](#markdown-header-options)
    * [validateDirectory(directory, createIfNotExist)](#markdown-header-validatedirectorydirectory-createifnotexist-string) ⇒ String ⏏
    * [buildCredentials(server, options)](#markdown-header-buildcredentialsserver-options-object) ⇒ Object ⏏
    * [initializeGlobalOptions(options, filesPattern)](#markdown-header-initializeglobaloptionsoptions-filespattern-object) ⇒ Object ⏏
    * [parseDownloadOptions(options, localDirectory, filesPattern)](#markdown-header-parsedownloadoptionsoptions-localdirectory-filespattern-object) ⇒ Object ⏏
    * [parseUploadOptions(options, localDirectory, filesPattern)](#markdown-header-parseuploadoptionsoptions-localdirectory-filespattern-object) ⇒ Object ⏏
    * [parseTransferOptions(options, filesPattern)](#markdown-header-parsetransferoptionsoptions-filespattern-object) ⇒ Object ⏏

### validateDirectory(directory, createIfNotExist) ⇒ String ⏏
Validate that the given {directory} exists and is not a file

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| directory | String | 
| createIfNotExist | Boolean | 

### buildCredentials(server, options) ⇒ Object ⏏
Build an object with all credentials for the given {server} based on given {options}

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| server | String | 
| options | Object | 

### initializeGlobalOptions(options, filesPattern) ⇒ Object ⏏
Validate the given {options} and {filePattern} inputs and create an options object based on that

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| options | Object | The options sent to the tool (through CLI or JS interfaces) |
| filesPattern | String ⎮ Array | The directories or files to handle |

### parseDownloadOptions(options, localDirectory, filesPattern) ⇒ Object ⏏
Parse options for the download method

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| options | Object | The options sent to the tool (through CLI or JS interfaces) |
| localDirectory | String | The local directory used for download method |
| filesPattern | String ⎮ Array | The directories or files to handle |

### parseUploadOptions(options, localDirectory, filesPattern) ⇒ Object ⏏
Parse options for the upload method

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| options | Object | The options sent to the tool (through CLI or JS interfaces) |
| localDirectory | String | The local directory used for upload method |
| filesPattern | String ⎮ Array | The directories or files to handle |

### parseTransferOptions(options, filesPattern) ⇒ Object ⏏
Parse options for the transfer method

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| options | Object | The options sent to the tool (through CLI or JS interfaces) |
| filesPattern | String ⎮ Array | The directories or files to handle |

## webdav
WebDAV helper


* [webdav](#markdown-header-webdav)
    * [flatten(list)](#markdown-header-flattenlist-array) ⇒ Array ⏏
    * [filter(list, type)](#markdown-header-filterlist-type-array) ⇒ Array ⏏
    * [isHTTPS(str)](#markdown-header-ishttpsstr-boolean) ⇒ Boolean ⏏
    * [getRequestOptions(credentials, endpoint)](#markdown-header-getrequestoptionscredentials-endpoint-object) ⇒ Object ⏏
    * [getLocalFullPath(directory, filename)](#markdown-header-getlocalfullpathdirectory-filename-string) ⇒ String ⏏
    * [getDirectoryOrFileStat(remotePath)](#markdown-header-getdirectoryorfilestatremotepath-promise) ⇒ Promise ⏏
    * [isFileExistsOnServer(fileStat, credentials)](#markdown-header-isfileexistsonserverfilestat-credentials-promise) ⇒ Promise ⏏
    * [filterFiles(fileStats, credentials)](#markdown-header-filterfilesfilestats-credentials-promise) ⇒ Promise ⏏
    * [obtainFilesListInDirectories(directoriesStats, onlyNewFiles, credentials)](#markdown-header-obtainfileslistindirectoriesdirectoriesstats-onlynewfiles-credentials-promise) ⇒ Promise ⏏
    * [obtainFullFilesList(directoriesOrFiles, recursive, onlyNewFiles, credentials)](#markdown-header-obtainfullfileslistdirectoriesorfiles-recursive-onlynewfiles-credentials-promise) ⇒ Promise ⏏
    * [downloadFile(localDirectory, fileStat, credentials)](#markdown-header-downloadfilelocaldirectory-filestat-credentials-promise) ⇒ Promise ⏏
    * [downloadFiles(localDirectory, fileList, credentials)](#markdown-header-downloadfileslocaldirectory-filelist-credentials-promise) ⇒ Promise ⏏
    * [uploadFile(fileStat, credentials)](#markdown-header-uploadfilefilestat-credentials-promise) ⇒ Promise ⏏
    * [uploadFiles(fileList, credentials)](#markdown-header-uploadfilesfilelist-credentials-promise) ⇒ Promise ⏏
    * [download(params)](#markdown-header-downloadparams-promise) ⇒ Promise ⏏
    * [upload(params)](#markdown-header-uploadparams-promise) ⇒ Promise ⏏

### flatten(list) ⇒ Array ⏏
Flat the given array and its child arrays in one array

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| list | Array | 

### filter(list, type) ⇒ Array ⏏
Filter the given {list} if the given {type}

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| list | Array | 
| type | String | 

### isHTTPS(str) ⇒ Boolean ⏏
Regexp that can checks if the given string is an HTTPS URL

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| str | String | 

### getRequestOptions(credentials, endpoint) ⇒ Object ⏏
Build the options object required by the request module related to the {credentials} and the {endpoint}

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| credentials | Object | 
| endpoint | String | 

### getLocalFullPath(directory, filename) ⇒ String ⏏
Returns the absolute local path for the given {filename} relative to the given {directory}
If the path does not exists yet, also recursively create it

**Kind**: Exported function  

| Param | Type |
| --- | --- |
| directory | String | 
| filename | String | 

### getDirectoryOrFileStat(remotePath) ⇒ Promise ⏏
Fetch the WebDAV server to get the directory or file stats related to the given {remotePath}

**Kind**: Exported function  
**Fulfil**: Object - Returns the file stat of the file  
**Reject**: Error - The error that occured  

| Param | Type |
| --- | --- |
| remotePath | String | 

### isFileExistsOnServer(fileStat, credentials) ⇒ Promise ⏏
Verify that the file exists or not on the server related to the given {credentials}

**Kind**: Exported function  
**Fulfil**: Object - Returns the file path if the file exists on the server related to the given {credentials}  

| Param | Type |
| --- | --- |
| fileStat | Object | 
| credentials | Object | 

### filterFiles(fileStats, credentials) ⇒ Promise ⏏
Filter the list of files if we only want to files that does not exists on the target server

**Kind**: Exported function  
**Fulfil**: Object - Returns the list of file stats to download  
**Reject**: Error - The error that occured  

| Param | Type |
| --- | --- |
| fileStats | Array | 
| credentials | Object | 

### obtainFilesListInDirectories(directoriesStats, onlyNewFiles, credentials) ⇒ Promise ⏏
Recursively returns the list of child files of the given list of directories

**Kind**: Exported function  
**Fulfil**: Array - Returns the list of found files (an array of each file stat)  
**Reject**: Error - The error that occured  

| Param | Type |
| --- | --- |
| directoriesStats | Array | 
| onlyNewFiles | Boolean | 
| credentials | Object | 

### obtainFullFilesList(directoriesOrFiles, recursive, onlyNewFiles, credentials) ⇒ Promise ⏏
Recursively loop over given directories or files list and returns the list of all files inside them.

**Kind**: Exported function  
**Fulfil**: Array - Returns the list of found files (an array of each file stat)  
**Reject**: Error - Returns the error that occured  

| Param | Type |
| --- | --- |
| directoriesOrFiles | Array | 
| recursive | Boolean | 
| onlyNewFiles | Boolean | 
| credentials | Object | 

### downloadFile(localDirectory, fileStat, credentials) ⇒ Promise ⏏
Download the file related to the given {fileStat} in the {localDirectory}

**Kind**: Exported function  
**Fulfil**: Object - Returns file stat  
**Reject**: Object - Returns the error wrapped in an object  

| Param | Type |
| --- | --- |
| localDirectory | String | 
| fileStat | Object | 
| credentials | Object | 

### downloadFiles(localDirectory, fileList, credentials) ⇒ Promise ⏏
Download the given {fileList} to the given {localDirectory} and respect the folder hierarchy

**Kind**: Exported function  
**Fulfil**: Array - Returns the list of downloaded files (even those that contains errors)  
**Reject**: Error - Returns the error that occured  

| Param | Type |
| --- | --- |
| localDirectory | String | 
| fileList | Array | 
| credentials | Object | 

### uploadFile(fileStat, credentials) ⇒ Promise ⏏
Upload the file related to the given {fileStat} to the server initliazed in the webdavClient variable

**Kind**: Exported function  
**Fulfil**: Object - Returns file stat  
**Reject**: Object - Returns the error wrapped in an object  

| Param | Type |
| --- | --- |
| fileStat | Object | 
| credentials | Object | 

### uploadFiles(fileList, credentials) ⇒ Promise ⏏
Upload the given {fileList} to the server initliazed in the webdavClient variable

**Kind**: Exported function  
**Fulfil**: Array - Returns the list of downloaded files (even those that contains errors)  
**Reject**: Error - Returns the error that occured  

| Param | Type |
| --- | --- |
| fileList | Array | 
| credentials | Object | 

### download(params) ⇒ Promise ⏏
Download the files from the instance related to the given {credentials} object in the given {localDirectory}.
Only download recursively the directories and files from the {directoriesAndFilesToDownload} pattern
If the param {onlyNewFiles} is sent as true, this method will only download files that does not exists on the target server (in case of transfer method)

**Kind**: Exported function  
**Fulfil**: Array - Returns the list of downloaded files (only successful ones)  
**Reject**: Error - Returns the error that occured  

| Param | Type |
| --- | --- |
| params | Object | 

### upload(params) ⇒ Promise ⏏
Upload the given {filesToUpload} to the instance related to the given {credentials} object from the given {localDirectory}.

**Kind**: Exported function  
**Fulfil**: undefined - Returns undefined when everything goes well  
**Reject**: Error - Returns the error that occured  

| Param | Type |
| --- | --- |
| params | Object | 

