## index
Javascript interface for the WebDAV Bulk Transfer tool


* [index](#markdown-header-index)
    * [module.exports.download(sourceURL, localdirectory, directoriesAndFiles, options)](#markdown-header-moduleexportsdownloadsourceurl-localdirectory-directoriesandfiles-options-promise) ⇒ Promise ⏏
    * [module.exports.upload(localdirectory, targetURL, options)](#markdown-header-moduleexportsuploadlocaldirectory-targeturl-options-promise) ⇒ Promise ⏏
    * [module.exports.transfer(sourceURL, targetURL, directoriesAndFiles, options)](#markdown-header-moduleexportstransfersourceurl-targeturl-directoriesandfiles-options-promise) ⇒ Promise ⏏

### module.exports.download(sourceURL, localdirectory, directoriesAndFiles, options) ⇒ Promise ⏏
Javascript interface to download directories and files from a webDAV server to a local directory

**Kind**: Exported function  
**Resolve**: Array If everything goes well, returns the list of downloaded files  
**Reject**: Error Returns an error if one happened  

| Param | Type | Description |
| --- | --- | --- |
| sourceURL | String | The source WebDAV URL |
| localdirectory | String | The local directory where the downloaded files will be stored before uploading them |
| directoriesAndFiles | String | A comma separated list of directories and files to transfer |
| options | Object | Object containing all possible options |

**Example**  
```js
// This command will download directories and files recursively from the given server to the given local directory (10 concurrent connections will be opened each time)
const webdavBulkTransfer = require('webdav-bulk-transfer');

webdavBulkTransfer.download('https://server.url', 'path/to/local/directory', ['/path/to/remote/directory','/path/to/remote/file.ext'], {
    username: 'username',
    password: 'password',
    recursive: true,
    concurrency: 10
});
```
### module.exports.upload(localdirectory, targetURL, options) ⇒ Promise ⏏
Javascript interface to upload directories and files from a local directory to a webDAV server

**Kind**: Exported function  
**Resolve**: Array If everything goes well, returns the list of uploaded files  
**Reject**: Error Returns an error if one happened  

| Param | Type | Description |
| --- | --- | --- |
| localdirectory | String | The local directory where the downloaded files will be stored before uploading them |
| targetURL | String | The target WebDAV URL |
| options | Object | Object containing all possible options |

**Example**  
```js
// This command will upload files in the local directory recursively to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
const webdavBulkTransfer = require('webdav-bulk-transfer');

webdavBulkTransfer.upload('path/to/local/directory', 'https://server.url', {
    username: 'username',
    password: 'password',
    recursive: true,
    concurrency: 10,
    cleanafter: true
});
```
### module.exports.transfer(sourceURL, targetURL, directoriesAndFiles, options) ⇒ Promise ⏏
Javascript interface to transfer directories and files from one webDAV server to another

**Kind**: Exported function  
**Resolve**: Array If everything goes well, returns the list of transfered files  
**Reject**: Error Returns an error if one happened  

| Param | Type | Description |
| --- | --- | --- |
| sourceURL | String | The source WebDAV URL |
| targetURL | String | The target WebDAV URL |
| directoriesAndFiles | String | A comma separated list of directories and files to transfer |
| options | Object | Object containing all possible options |

**Example**  
```js
// This command will transfer files recursively from the given source server to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
const webdavBulkTransfer = require('webdav-bulk-transfer');

webdavBulkTransfer.transfer('https://source.server', 'https://targer.server', ['/path/to/remote/directory','/path/to/remote/file.ext'], {
    sourceusername: 'username',
    sourcepassword: 'password',
    targetusername: 'username',
    targetpassword: 'password',
    recursive: true,
    concurrency: 10,
    cleanafter: true,
    onlynewfiles: true,
    keeplocaldirectory: true
});
```
