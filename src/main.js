'use strict';

/**
 * Main transfer module
 * @module main
 */

/**
 * @type {lib/log}
 */
const log = require('../lib/log');
/**
 * @type {lib/helper}
 */
const helper = require('../lib/helper');
/**
 * @type {lib/options}
 */
const options = require('../lib/options');
/**
 * @type {lib/webdav}
 */
const webdav = require('../lib/webdav');

/**
 * This method will download all directories and files in the given {filesPattern} locally from the source instance
 *
 * @alias module:main
 *
 * @param {String} source The source WebDAV URL
 * @param {string} localDirectory The local directory where to download files
 * @param {String|Array} filesPattern The directories or files to transfer
 * @param {Object} params
 *
 * @returns {Promise}
 * @fulfil {undefined} - Returns undefined when everything goes well
 * @reject {Error} - The error that occured
 */
function download(source, localDirectory, filesPattern, params) {
    return new Promise((resolve, reject) => {
        if (source === undefined || source.length === 0 || localDirectory === undefined || localDirectory.length === 0) {
            reject(new Error('Missing source or localDirectory parameters.'));
        }

        if (filesPattern === undefined || filesPattern.length === 0) {
            reject(new Error('No directories or files given to transfer.'));
        }

        try {
            params = options.parseDownloadOptions(params, localDirectory, filesPattern);
        } catch (e) {
            log.error(e);
            reject(e);
            return;
        }

        params.sourceCredentials.hostname = source;

        log.info('Start download process.');

        // initial empty promise
        // eslint-disable-next-line no-unused-vars
        var promise = Promise.resolve();

        promise = promise.then(() => params.keepLocalDirectory === true ? undefined : helper.cleanDirectory(params.localDirectory));
        promise = promise.then(() => webdav.download(params));
        promise = promise.then(downloadedFiles => {
            log.success('End download process.');
            resolve(downloadedFiles);
        });
        promise = promise.catch(err => {
            log.error(err);
            reject(err);
        });
    });
}

/**
 * This method will download all directories and files in the given {filesPattern} locally from the source instance
 * Then upload all those directories files to the target instance in the exact same structure.
 *
 * @alias module:main
 *
 * @param {String} localDirectory The local directory where the downloaded files will be stored before uploading them
 * @param {String} target The target WebDAV URL
 * @param {Object} params
 *
 * @returns {Promise}
 * @fulfil {undefined} - Returns undefined when everything goes well
 * @reject {Error} - The error that occured
 */
function upload(localDirectory, target, params) {
    return new Promise((resolve, reject) => {
        if (target === undefined || target.length === 0) {
            reject(new Error('Missing target parameters.'));
        }

        if (localDirectory === undefined || localDirectory.length === 0) {
            reject(new Error('No directories or files given to upload.'));
        }

        try {
            params = options.parseUploadOptions(params, localDirectory);
        } catch (e) {
            log.error(e);
            reject(e);
            return;
        }

        params.targetCredentials.hostname = target;

        // get only files to upload
        let filesToUpload = helper.getFilesRecursively(localDirectory, undefined, params.recursive);

        log.info('Start upload process.');

        // initial empty promise
        // eslint-disable-next-line no-unused-vars
        var promise = Promise.resolve();

        promise = promise.then(() => webdav.upload(params, filesToUpload));
        promise = promise.then(uploadedFiles => {
            if (params.cleanAfter === true) {
                return helper.cleanDirectory(params.localDirectory).then(() => uploadedFiles);
            }

            return uploadedFiles;
        });
        promise = promise.then(uploadedFiles => {
            log.success('End upload process.');
            resolve(uploadedFiles);
        });
        promise = promise.catch(err => {
            log.error(err);
            reject(err);
        });
    });
}

/**
 * This method will download all directories and files in the given {filesPattern} locally from the source instance
 * Then upload all those directories files to the target instance in the exact same structure.
 *
 * @alias module:main
 *
 * @param {String} source The source WebDAV URL
 * @param {String} target The target WebDAV URL
 * @param {String|Array} filesPattern The directories or files to transfer
 * @param {Object} params
 *
 * @returns {Promise}
 * @fulfil {undefined} - Returns undefined when everything goes well
 * @reject {Error} - The error that occured
 */
function transfer(source, target, filesPattern, params) {
    return new Promise((resolve, reject) => {
        if (source === undefined || source.length === 0 || target === undefined || target.length === 0) {
            reject(new Error('Missing source or target parameters.'));
        }

        if (filesPattern === undefined || filesPattern.length === 0) {
            reject(new Error('No directories or files given to transfer.'));
        }

        try {
            params = options.parseTransferOptions(params, filesPattern);
        } catch (e) {
            reject(e);
        }

        params.sourceCredentials.hostname = source;
        params.targetCredentials.hostname = target;

        log.info('Start transfer process.');

        // initial empty promise
        // eslint-disable-next-line no-unused-vars
        var promise = Promise.resolve();

        promise = promise.then(() => params.keepLocalDirectory === true ? undefined : helper.cleanDirectory(params.localDirectory));
        promise = promise.then(() => webdav.download(params));
        promise = promise.then(downloadedFiles => webdav.upload(params, downloadedFiles));
        promise = promise.then(uploadedFiles => {
            if (params.cleanAfter === true) {
                return helper.cleanDirectory(params.localDirectory).then(() => uploadedFiles);
            }

            return uploadedFiles;
        });
        promise = promise.then(uploadedFiles => {
            log.success('End transfer process.');
            resolve(uploadedFiles);
        });
        promise = promise.catch(err => {
            log.error(err);
            reject(err);
        });
    });
}

module.exports = {
    download: download,
    transfer: transfer,
    upload: upload
};
