'use strict';

/* eslint-disable no-console */

/**
 * Custom logger
 * @module log
 */

/**
 * @type {node_modules/chalk}
 */
const chalk = require('chalk');

/**
 * @type {Function}
 */
const i = chalk.blue;
/**
 * @type {Function}
 */
const w = chalk.yellow;
/**
 * @type {Function}
 */
const e = chalk.red;
/**
 * @type {Function}
 */
const s = chalk.green;

/**
 * @type {Object}
 */
const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
};
/**
 * @type {String}
 */
const locale = 'en-US';

/**
 * Returns true if the tool has to be quiet (meaning not logging everything)
 *
 * @alias module:log
 *
 * @param {Boolean} checkOnlyRunningMode only checks the running mode (not the environment variables)
 *
 * @returns {Boolean}
 */
function isQuiet(checkOnlyRunningMode) {
    checkOnlyRunningMode = checkOnlyRunningMode !== undefined ? checkOnlyRunningMode : false;

    if (checkOnlyRunningMode === true) {
        return process.env.runningAsScript !== undefined && [true, 'true'].indexOf(process.env.runningAsScript) > -1;
    }

    return (process.env.quiet !== undefined && [true, 'true'].indexOf(process.env.quiet) > -1)
        || (process.env.runningAsScript !== undefined && [true, 'true'].indexOf(process.env.runningAsScript) > -1);
}

/**
 * Log the given message as a info message
 *
 * @alias module:log
 */
function info() {
    if (isQuiet()) {
        return;
    }

    let d = new Date();

    arguments[0] = i(d.toLocaleDateString(locale, options), 'Info:', arguments[0]);
    console.info.apply(null, arguments);
    return true;
}

/**
 * Log the given message as a success message
 *
 * @alias module:log
 */
function success() {
    if (isQuiet(true)) {
        return;
    }

    let d = new Date();

    arguments[0] = s(d.toLocaleDateString(locale, options), 'Success: ', arguments[0]);
    console.log.apply(null, arguments);
    return true;
}

/**
 * Log the given message as a warn message
 *
 * @alias module:log
 */
function warn() {
    if (isQuiet()) {
        return;
    }

    let d = new Date();

    arguments[0] = w(d.toLocaleDateString(locale, options), 'Warning: ', arguments[0]);
    console.warn.apply(null, arguments);
    return true;
}

/**
 * Log the given message as a error message
 *
 * @alias module:log
 */
function error() {
    if (isQuiet(true)) {
        return;
    }

    let d = new Date();

    arguments[0] = e(d.toLocaleDateString(locale, options), 'Error: ', arguments[0]);
    console.error.apply(null, arguments);
    return true;
}

module.exports = {
    isQuiet,
    info: info,
    warn: warn,
    success: success,
    error: error
};

/* eslint-enable no-console */
