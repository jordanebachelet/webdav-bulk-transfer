'use strict';

/**
 * WebDAV helper
 * @module webdav
 */

/**
 * @type {node_modules/es6-promise-pool}
 */
const PromisePool = require('es6-promise-pool');
/**
 * @type {node_modules/fs}
 */
const fs = require('fs');
/**
 * @type {node_modules/path}
 */
const path = require('path');
/**
 * @type {node_modules/request}
 */
const request = require('request');
/**
 * @type {node_modules/webdav}
 */
const { createClient } = require('webdav');

/**
 * @type {lib/log}
 */
const log = require('./log');
/**
 * @type {lib/helper}
 */
const helper = require('./helper');

/**
 * Private webdav client
 *
 * @type {Object}
 */
var webdavClient;
/**
 * Access to program params
 *
 * @type {Object}
 */
var programParams;
/**
 * Private global reference to the param {simultaneaousPromises}
 *
 * @type {Integer}
 */
var SIMULTANEAOUS_PROMISES;

/**
 * Flat the given array and its child arrays in one array
 *
 * @alias module:webdav
 *
 * @param {Array} list
 *
 * @returns {Array}
 */
const flatten = list => list.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

/**
 * Filter the given {list} if the given {type}
 *
 * @alias module:webdav
 *
 * @param {Array} list
 * @param {String} type
 *
 * @returns {Array}
 */
const filter = (list, type) => list.filter(s => s.type === type);

/**
 * Regexp that can checks if the given string is an HTTPS URL
 *
 * @alias module:webdav
 *
 * @param {String} str
 *
 * @returns {Boolean}
 */
const isHTTPS = str => new RegExp('/^https:\/\//').test(str);

/**
 * Re-authenticate with the Wevdav client in case the connection has been lost
 */
function ensureWebdavConnection() {
    webdavClient = createClient(`${programParams.sourceCredentials.hostname}`, {
        username: programParams.sourceCredentials.username,
        password: programParams.sourceCredentials.password
    });
}

/**
 * Build the options object required by the request module related to the {credentials} and the {endpoint}
 *
 * @alias module:webdav
 *
 * @param {Object} credentials
 * @param {String} endpoint
 *
 * @returns {Object}
 */
function getRequestOptions(credentials, endpoint) {
    // Encode each part of the path, but not the path.sep themselves
    endpoint = endpoint.split(path.sep).map(function (part) {
        return encodeURIComponent(part);
    }).join(path.sep);

    var opts = {
        baseUrl: `${credentials.hostname}`,
        uri: endpoint,
        strictSSL: isHTTPS(credentials.hostname)
    };

    if (credentials.username !== undefined && credentials.password !== undefined) {
        opts.auth = {
            username: credentials.username,
            password: credentials.password
        };
    }

    if (credentials.certificate !== undefined) {
        opts.agentOptions = {
            pfx: credentials.certificate,
            passphrase: credentials.passphrase // as passphrase is optional, it can be undefined here
        };
    }

    return opts;
}

/**
 * Returns the absolute local path for the given {filename} relative to the given {directory}
 * If the path does not exists yet, also recursively create it
 *
 * @alias module:webdav
 *
 * @param {String} directory
 * @param {String} filename
 *
 * @returns {String}
 */
function getLocalFullPath(directory, filename) {
    let localFolder = filename.split(path.sep);
    let basename = localFolder.pop();
    localFolder = path.join(directory, localFolder.join(path.sep));
    if (!fs.existsSync(localFolder)) {
        helper.mkdirRecursive(localFolder.replace(process.cwd(), ''));
    }

    return path.join(localFolder, basename);
}

/**
 * Fetch the WebDAV server to get the directory or file stats related to the given {remotePath}
 *
 * @alias module:webdav
 *
 * @param {String} remotePath
 *
 * @returns {Promise}
 * @fulfil {Object} - Returns the file stat of the file
 * @reject {Error} - The error that occured
 */
function getDirectoryOrFileStat(remotePath) {
    if (!remotePath) {
        return null;
    }

    return new Promise((resolve, reject) => {
        try {
            webdavClient.stat(remotePath).then(resolve);
        } catch (e) {
            ensureWebdavConnection();
            webdavClient.stat(remotePath).then(resolve).catch(err => reject(err));
        }
    });
}

/**
 * Verify that the file exists or not on the server related to the given {credentials}
 *
 * @alias module:webdav
 *
 * @param {Object} fileStat
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Object} - Returns the file path if the file exists on the server related to the given {credentials}
 */
function isFileExistsOnServer(fileStat, credentials) {
    if (!fileStat) {
        return null;
    }

    return new Promise(resolve => {
        var options = getRequestOptions(credentials, fileStat.filename);
        options.method = 'HEAD';

        try {
            log.info(`Checking "${fileStat.filename}" remotely...`);
            request(options, (err, res) => {
                if (res && res.statusCode === 200) {
                    // Catch directories
                    if (res.body.indexOf('<title>Index of') > -1) {
                        resolve(fileStat);
                        return;
                    }

                    log.warn(`The file "${fileStat.filename}" already exists on the server "${credentials.hostname}". Skip this file...`);
                    resolve(undefined);
                    return;
                }

                log.success(`The file "${fileStat.filename}" does not exist on the remote server "${credentials.hostname}"...`);
                resolve(fileStat);
            });
        } catch (err) {
            resolve(fileStat);
        }
    });
}

/**
 * Filter the list of files if we only want to files that does not exists on the target server
 *
 * @alias module:webdav
 *
 * @param {Array} fileStats
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Object} - Returns the list of file stats to download
 * @reject {Error} - The error that occured
 */
function filterFiles(fileStats, credentials) {
    return new Promise((resolve, reject) => {
        if (fileStats.length === 0) {
            resolve(fileStats);
            return;
        }

        var files = [];
        function generatePromises() {
            return isFileExistsOnServer(fileStats.shift(), credentials);
        }

        var pool = new PromisePool(generatePromises, Math.min(SIMULTANEAOUS_PROMISES, fileStats.length));
        pool.addEventListener('fulfilled', event => {
            if (event.data.result === undefined) {
                return;
            }
            files.push(event.data.result);
        });

        pool.start().then(() => {
            resolve(files);
        }).catch(err => reject(err));
    });
}

/**
 * Filter the given {files} list with only the files which don't exist yes locally
 *
 * @param {Array} files Array of file stats to filter out
 * @param {String} localDirectory The local directory where files are stored, and downloaded to/uplodated from
 *
 * @returns {Array}
 */
function filterExistingLocalFiles(files, localDirectory) {
    return files.filter(fileStat => !fs.existsSync(getLocalFullPath(localDirectory, fileStat.filename)));
}

/**
 * Recursively returns the list of child files of the given list of directories
 *
 * @alias module:webdav
 *
 * @param {Array} directoriesStats
 * @param {Boolean} onlyNewFiles
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Array} - Returns the list of found files (an array of each file stat)
 * @reject {Error} - The error that occured
 */
function obtainFilesListInDirectories(directoriesStats, onlyNewFiles, credentials, localDirectory) {
    var files = [];

    return new Promise((resolve, reject) => {
        if (directoriesStats.length === 0) {
            resolve(files);
            return;
        }

        var dirOrFileStats = [];
        function generatePromises() {
            let item = directoriesStats.shift();
            try {
                return item !== undefined ? webdavClient.getDirectoryContents(item.filename) : null;
            } catch (e) {
                ensureWebdavConnection();
                return item !== undefined ? webdavClient.getDirectoryContents(item.filename) : null;
            }
        }

        var pool = new PromisePool(generatePromises, Math.min(SIMULTANEAOUS_PROMISES, directoriesStats.length));
        pool.addEventListener('fulfilled', event => {
            if (event.data.result === undefined) {
                return;
            }
            dirOrFileStats.push(event.data.result);
        });

        pool.start().then(async () => {
            dirOrFileStats = flatten(dirOrFileStats);
            files = files.concat(filter(dirOrFileStats, 'file'));

            // Filter out already downloaded files
            files = filterExistingLocalFiles(Array.from(files), localDirectory);

            if (onlyNewFiles === true) {
                files = await filterFiles(files, credentials);
            }

            var subDirectories = filter(dirOrFileStats, 'directory');
            if (subDirectories.length === 0) {
                resolve(files);
                return;
            }

            obtainFilesListInDirectories(subDirectories, onlyNewFiles, credentials, localDirectory).then(subDirOrFilesStats => {
                log.success(`Resolved "${subDirOrFilesStats.length}" files in the sub directories...`);
                resolve(files.concat(subDirOrFilesStats));
            });
        }).catch(err => reject(err));
    });
}

/**
 * Recursively loop over given directories or files list and returns the list of all files inside them.
 *
 * @alias module:webdav
 *
 * @param {Array} directoriesOrFiles
 * @param {Boolean} recursive
 * @param {Boolean} onlyNewFiles
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Array} - Returns the list of found files (an array of each file stat)
 * @reject {Error} - Returns the error that occured
 */
function obtainFullFilesList(directoriesOrFiles, recursive, onlyNewFiles, credentials, localDirectory) {
    var files = [];

    return new Promise((resolve, reject) => {
        if (directoriesOrFiles.length === 0) {
            resolve(files);
            return;
        }

        var dirOrFileStats = [];
        function generatePromises() {
            return getDirectoryOrFileStat(directoriesOrFiles.shift());
        }

        var pool = new PromisePool(generatePromises, Math.min(SIMULTANEAOUS_PROMISES, directoriesOrFiles.length));
        pool.addEventListener('fulfilled', event => {
            if (event.data.result === undefined) {
                return;
            }

            dirOrFileStats.push(event.data.result);
        });

        pool.start().then(async () => {
            files = files.concat(filter(dirOrFileStats, 'file'));

            // Filter out already downloaded files
            files = filterExistingLocalFiles(Array.from(files), localDirectory);

            if (onlyNewFiles === true) {
                files = await filterFiles(files, credentials);
            }

            var subDirectories = filter(dirOrFileStats, 'directory');
            if (recursive === false || subDirectories.length === 0) {
                resolve(files);
                return;
            }

            obtainFilesListInDirectories(subDirectories, onlyNewFiles, credentials, localDirectory).then(subFiles => {
                log.success(`Resolved "${subFiles.length}" files in the sub directories...`);
                resolve(files.concat(subFiles));
            }).catch(err => reject(err));
        }).catch(err => reject(err));
    });
}

/**
 * Download the file related to the given {fileStat} in the {localDirectory}
 *
 * @alias module:webdav
 *
 * @param {String} localDirectory
 * @param {Object} fileStat
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Object} - Returns file stat
 * @reject {Object} - Returns the error wrapped in an object
 */
function downloadFile(localDirectory, fileStat, credentials) {
    return new Promise(resolve => {
        let remoteFilePath = fileStat.filename;
        let localFullPath = getLocalFullPath(localDirectory, remoteFilePath);

        log.info(`Downloading the file "${credentials.hostname}${remoteFilePath}" to "${localFullPath}".`);

        var options = getRequestOptions(credentials, remoteFilePath);

        try {
            request.get(options).pipe(fs.createWriteStream(localFullPath)).on('finish', () => {
                log.success(`"${options.baseUrl}${remoteFilePath}" successfully downloaded to "${localFullPath}".`);

                resolve(Object.assign(fileStat, {
                    success: true,
                    localFilename: localFullPath
                }));
            }).on('error', err => {
                resolve({
                    success: false,
                    error: err
                });
            });
        } catch (err) {
            resolve({
                success: false,
                error: err
            });
        }
    });
}

/**
 * Download the given {fileList} to the given {localDirectory} and respect the folder hierarchy
 *
 * @alias module:webdav
 *
 * @param {String} localDirectory
 * @param {Array} fileList
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Array} - Returns the list of downloaded files (even those that contains errors)
 * @reject {Error} - Returns the error that occured
 */
function downloadFiles(localDirectory, fileList, credentials) {
    log.info(`${fileList.length} to download from "${credentials.hostname}".`);

    return new Promise((resolve, reject) => {
        var downloadedFiles = [];
        function generatePromises() {
            let item = fileList.shift();
            return item !== undefined ? downloadFile(localDirectory, item, credentials) : null;
        }

        var pool = new PromisePool(generatePromises, SIMULTANEAOUS_PROMISES);
        pool.addEventListener('fulfilled', event => {
            if (event.data.result === undefined) {
                return;
            }
            downloadedFiles.push(event.data.result);
        });

        pool.start().then(() => {
            resolve(downloadedFiles.filter(f => f.success === true));
        }).catch(err => reject(err));
    });
}

/**
 * Upload the file related to the given {fileStat} to the server initliazed in the webdavClient variable
 *
 * @alias module:webdav
 *
 * @param {Object} fileStat
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Object} - Returns file stat
 * @reject {Object} - Returns the error wrapped in an object
 */
function uploadFile(fileStat, credentials) {
    return new Promise(resolve => {
        log.info(`Uploading the file "${fileStat.localFilename}" to "${credentials.hostname}${fileStat.filename}".`);

        var options = getRequestOptions(credentials, fileStat.filename);

        try {
            fs.createReadStream(fileStat.localFilename).pipe(request.put(options)).on('end', () => {
                log.success(`"${fileStat.localFilename}" successfully uploaded to "${options.baseUrl}${options.uri}".`);
                resolve({
                    success: true
                });
            }).on('error', err => {
                resolve({
                    success: false,
                    error: err
                });
            });
        } catch (err) {
            resolve({
                success: false,
                error: err
            });
        }
    });
}

/**
 * Upload the given {fileList} to the server initliazed in the webdavClient variable
 *
 * @alias module:webdav
 *
 * @param {Array} fileList
 * @param {Object} credentials
 *
 * @returns {Promise}
 * @fulfil {Array} - Returns the list of downloaded files (even those that contains errors)
 * @reject {Error} - Returns the error that occured
 */
function uploadFiles(fileList, credentials) {
    log.info(`${fileList.length} files to upload to "${credentials.hostname}".`);

    return new Promise((resolve, reject) => {
        if (fileList.length === 0) {
            resolve(fileList);
            return;
        }

        var uploadedFiles = [];
        function generatePromises() {
            let item = fileList.shift();
            return item !== undefined ? uploadFile(item, credentials) : null;
        }

        var pool = new PromisePool(generatePromises, SIMULTANEAOUS_PROMISES);
        pool.addEventListener('fulfilled', event => {
            if (event.data.result === undefined) {
                return;
            }
            uploadedFiles.push(event.data.result);
        });

        pool.start().then(() => {
            resolve(uploadedFiles.filter(res => res && res.success));
        }).catch(err => reject(err));
    });
}

/**
 * Download the files from the instance related to the given {credentials} object in the given {localDirectory}.
 * Only download recursively the directories and files from the {directoriesAndFilesToDownload} pattern
 * If the param {onlyNewFiles} is sent as true, this method will only download files that does not exists on the target server (in case of transfer method)
 *
 * @alias module:webdav
 *
 * @param {Object} params
 *
 * @returns {Promise}
 * @fulfil {Array} - Returns the list of downloaded files (only successful ones)
 * @reject {Error} - Returns the error that occured
 */
function download(params) {
    return new Promise((resolve, reject) => {
        if (params.sourceCredentials === undefined || params.localDirectory === undefined || params.localDirectory.length === 0) {
            reject(new Error('Bad parameters, cannot download any files.'));
            return;
        }

        if (params.directoriesAndFilesToDownload.length === 0) {
            log.success('Nothing to upload.');
            resolve(undefined);
            return;
        }

        log.info(`Start downloading directories and files from "${params.sourceCredentials.hostname}" to "${params.localDirectory}".`);

        // Connect to WebDAV
        programParams = params;
        ensureWebdavConnection();
        SIMULTANEAOUS_PROMISES = params.concurrency;

        // initial empty promise
        // eslint-disable-next-line no-unused-vars
        var promise = Promise.resolve();
        promise = promise.then(() => obtainFullFilesList(params.directoriesAndFilesToDownload, params.recursive, params.onlyNewFiles, params.targetCredentials, params.localDirectory));
        promise = promise.then(fileStats => downloadFiles(params.localDirectory, fileStats, params.sourceCredentials));
        promise = promise.then(downloadedFiles => {
            downloadedFiles = downloadedFiles.filter(fileStat => {
                if (fileStat.success === false) {
                    log.error(`Unable to download the file "${fileStat.filename}". Error: ${fileStat.error}`);
                }

                return fileStat.success;
            });

            log.success(`${downloadedFiles.length} files successfully downloaded from "${params.sourceCredentials.hostname}".`);
            resolve(downloadedFiles);
        });
        promise = promise.catch(err => reject(err));
    });
}

/**
 * Upload the given {filesToUpload} to the instance related to the given {credentials} object from the given {localDirectory}.
 *
 * @alias module:webdav
 *
 * @param {Object} params
 *
 * @returns {Promise}
 * @fulfil {undefined} - Returns undefined when everything goes well
 * @reject {Error} - Returns the error that occured
 */
function upload(params, filesToUpload) {
    return new Promise((resolve, reject) => {
        if (params.targetCredentials === undefined || params.localDirectory === undefined || params.localDirectory.length === 0) {
            reject(new Error('Bad parameters, cannot upload any files.'));
            return;
        }

        if (filesToUpload.length === 0) {
            log.success('Nothing to upload.');
            resolve(undefined);
            return;
        }

        log.info(`Uploading files from "${params.localDirectory}" to "${params.targetCredentials.hostname}".`);
        SIMULTANEAOUS_PROMISES = params.concurrency;

        // initial empty promise
        // eslint-disable-next-line no-unused-vars
        var promise = Promise.resolve();
        promise = promise.then(() => params.onlyNewFiles !== undefined && params.onlyNewFiles === true ? filterFiles(filesToUpload, params.targetCredentials) : filesToUpload);
        promise = promise.then(files => uploadFiles(files, params.targetCredentials, params.onlyNewFiles));
        promise = promise.then(uploadedFiles => {
            uploadedFiles = uploadedFiles.filter(fileStat => {
                if (fileStat.success === false) {
                    log.error(`Unable to upload the file "${fileStat.filename}". Error: ${fileStat.error}`);
                }

                return fileStat.success;
            });

            log.success(`${uploadedFiles.length} files successfully uploaded to "${params.targetCredentials.hostname}".`);
            resolve(undefined);
        });
        promise = promise.catch(err => reject(err));
    });
}

module.exports = {
    download: download,
    upload: upload
};
