'use strict';

/**
 * Helper toolbox
 * @module helper
 */

/**
 * @type {node_modules/del}
 */
const del = require('del');
/**
 * @type {node_modules/fs}
 */
const fs = require('fs');
/**
 * @type {node_modules/path}
 */
const path = require('path');

/**
 * @type {lib/log}
 */
const log = require('./log');

/**
 * Create the hierarchy recursively for the given {directory}
 *
 * @alias module:util
 *
 * @param {String} directory The directory path to create recusively
 *
 * @returns {Boolean}
 **/
function mkdirRecursive(directory) {
    directory.split(path.sep).reduce((parentDir, childDir) => {
        let curDir = path.resolve(process.cwd(), parentDir, childDir);
        try {
            fs.mkdirSync(curDir);
        } catch (err) {
            if (err.code !== 'EEXIST') {
                throw err;
            }
        }

        return curDir;
    }, '');

    return true;
}

/**
 * Remove the given {directory} and all its content
 *
 * @alias module:helper
 *
 * @param {String} directory
 *
 * @returns {Promise}
 * @fulfil {undefined} - Returns undefined when everything goes well
 * @reject {Error} - The error that occured
 */
function cleanDirectory(directory) {
    return new Promise((resolve, reject) => {
        if (!fs.existsSync(directory)) {
            reject(new Error(`The directory "${directory}" does not exists. Abort.`));
        }

        log.info(`Clean the "${directory}" directory.`);

        del([directory]).then(paths => {
            paths.forEach(p => log.info(`"${p}" successfully cleaned.`));
            resolve(undefined);
        }).catch(err => reject(err));
    });
}

/**
 * Returns the list of files in the given {directoriesAndFiles} list recursively, or not
 *
 * @param {Array} directory
 * @param {Boolean} recursively
 *
 * @returns {Array}
 */
function getFilesRecursively(directory, initialDirectory, recursively) {
    let files = [];
    let directories = [];

    if (directory === undefined || directory.length === 0) {
        return files;
    }

    directory = path.resolve(process.cwd(), directory);
    initialDirectory = initialDirectory !== undefined ? initialDirectory : directory;

    fs.readdirSync(directory).forEach(dirOrFile => {
        let p = path.resolve(process.cwd(), directory + path.sep + dirOrFile);
        let stat = fs.statSync(p);

        if (stat.isFile()) {
            files.push({
                localFilename: p,
                filename: p.replace(initialDirectory, '')
            });
        } else {
            directories.push(p);
        }
    });

    if (recursively === true && directories.length > 0) {
        directories.forEach(directory => {
            files = files.concat(getFilesRecursively(directory, initialDirectory, recursively));
        });
    }

    return files;
}

module.exports = {
    cleanDirectory: cleanDirectory,
    getFilesRecursively: getFilesRecursively,
    mkdirRecursive: mkdirRecursive
};
