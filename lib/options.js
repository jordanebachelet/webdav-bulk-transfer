'use strict';

/**
 * Options parser
 * @module options
 */

/**
 * @type {node_modules/fs}
 */
const fs = require('fs');
/**
 * @type {node_modules/path}
 */
const path = require('path');

/**
 * @type {lib/helper}
 */
const helper = require('./helper');

/**
 * @type {Object}
 */
const DEFAULTS = {
    concurrency: 1,
    localDirectory: 'downloaded_files'
};

/**
 * Validate that the given {directory} exists and is not a file
 *
 * @alias module:options
 *
 * @param {String} directory
 * @param {Boolean} createIfNotExist
 *
 * @returns {String}
 */
function validateDirectory(directory, createIfNotExist) {
    if (directory === undefined || directory.length === 0) {
        directory = DEFAULTS.localDirectory;
    }

    let fullPath = path.resolve(process.cwd(), directory);

    if (createIfNotExist === true) {
        helper.mkdirRecursive(directory);
        return fullPath;
    }

    if (!fs.existsSync(fullPath)) {
        throw new Error(`The directory "${fullPath}" does not exists.`);
    }

    let stat = fs.statSync(fullPath);
    if (stat.isFile()) {
        throw new Error(`The directory "${directory}" cannot be a file.`);
    }

    return fullPath;
}

/**
 * Build an object with all credentials for the given {server} based on given {options}
 *
 * @alias module:options
 *
 * @param {String} server
 * @param {Object} options
 *
 * @returns {Object}
 */
function buildCredentials(server, options) {
    if (options === undefined) {
        return {};
    }

    let certificate;
    let certificateProp = server + 'certificate';
    let certificatePath = options[certificateProp];

    if (certificatePath !== undefined) {
        if (!fs.existsSync(certificatePath)) {
            throw new Error(`The given certificate file does not exists at path "${certificatePath}".`);
        } else {
            let stat = fs.statSync(certificatePath);
            if (!stat.isFile()) {
                throw new Error(`The given certificate is not a file at path "${certificatePath}".`);
            }

            certificate = fs.readFileSync(certificatePath);
        }
    }

    let username = options[server + 'username'];
    // Try to parse the given username in case the username is a stringified OAuth token object has been provided through the CLI interface
    try {
        if (typeof username === 'string') {
            username = JSON.parse(username);
        }
    } catch (e) {}

    return {
        username: username,
        password: typeof username === 'string' ? options[server + 'password'] : undefined,
        certificate: certificate,
        passphrase: options[server + 'passphrase']
    };
}

/**
 * Validate the given {options} and {filePattern} inputs and create an options object based on that
 *
 * @alias module:options
 *
 * @param {Object} options The options sent to the tool (through CLI or JS interfaces)
 * @param {String|Array} filesPattern The directories or files to handle
 *
 * @returns {Object}
 */
function initializeGlobalOptions(options, filesPattern) {
    if (options === undefined) {
        return DEFAULTS;
    }

    /**
     * @type {Object}
     */
    var opts = Object.assign({}, DEFAULTS);

    if (options.concurrency !== undefined) {
        let c = parseFloat(options.concurrency);
        if (!isNaN(c)) {
            opts.concurrency = c;
        }
    }
    if (options.localdirectory !== undefined) {
        opts.localDirectory = options.localdirectory;
    }
    opts.localDirectory = validateDirectory(opts.localDirectory, true);
    opts.quiet = options.quiet === true;
    opts.cleanAfter = options.cleanafter === true;
    opts.recursive = options.recursive !== undefined ? options.recursive : true;
    opts.onlyNewFiles = options.onlynewfiles === true;
    opts.keepLocalDirectory = options.keeplocaldirectory === true;

    // filesPattern option
    var directoriesAndFiles;
    if (typeof filesPattern === 'string') {
        directoriesAndFiles = filesPattern.split(',');
        if (directoriesAndFiles.length === 0) {
            throw new Error('No directories or files to transfer. Abort.');
        }
    } else if (typeof filesPattern === 'object' && Array.isArray(filesPattern)) {
        directoriesAndFiles = filesPattern;
    }
    opts.directoriesAndFilesToDownload = directoriesAndFiles;

    return opts;
}

/**
 * Parse options for the download method
 *
 * @alias module:options
 *
 * @param {Object} options The options sent to the tool (through CLI or JS interfaces)
 * @param {String} localDirectory The local directory used for download method
 * @param {String|Array} filesPattern The directories or files to handle
 *
 * @returns {Object}
 */
function parseDownloadOptions(options, localDirectory, filesPattern) {
    var opts = initializeGlobalOptions(options, filesPattern);

    opts.localDirectory = validateDirectory(localDirectory, true);
    opts.sourceCredentials = buildCredentials('', options);

    return opts;
}

/**
 * Parse options for the upload method
 *
 * @alias module:options
 *
 * @param {Object} options The options sent to the tool (through CLI or JS interfaces)
 * @param {String} localDirectory The local directory used for upload method
 * @param {String|Array} filesPattern The directories or files to handle
 *
 * @returns {Object}
 */
function parseUploadOptions(options, localDirectory, filesPattern) {
    var opts = initializeGlobalOptions(options, filesPattern);

    opts.localDirectory = validateDirectory(localDirectory, true);
    opts.targetCredentials = buildCredentials('', options);

    return opts;
}

/**
 * Parse options for the transfer method
 *
 * @alias module:options
 *
 * @param {Object} options The options sent to the tool (through CLI or JS interfaces)
 * @param {String|Array} filesPattern The directories or files to handle
 *
 * @returns {Object}
 */
function parseTransferOptions(options, filesPattern) {
    var opts = initializeGlobalOptions(options, filesPattern);

    opts.sourceCredentials = buildCredentials('source', options);
    opts.targetCredentials = buildCredentials('target', options);

    return opts;
}

module.exports = {
    parseDownloadOptions: parseDownloadOptions,
    parseUploadOptions: parseUploadOptions,
    parseTransferOptions: parseTransferOptions
};
