'use strict';

/**
 * Javascript interface for the WebDAV Bulk Transfer tool
 * @module index
 */

/**
 * @type {src/main}
 */
const main = require('../src/main');

/**
 * Javascript interface to download directories and files from a webDAV server to a local directory
 *
 * @alias module:index
 *
 * @param {String} sourceURL The source WebDAV URL
 * @param {String} localdirectory The local directory where the downloaded files will be stored before uploading them
 * @param {String} directoriesAndFiles A comma separated list of directories and files to transfer
 * @param {Object} options Object containing all possible options
 *
 * @returns {Promise}
 * @resolve {Array} If everything goes well, returns the list of downloaded files
 * @reject {Error} Returns an error if one happened
 *
 * @example
 * // This command will download directories and files recursively from the given server to the given local directory (10 concurrent connections will be opened each time)
 * const webdavBulkTransfer = require('webdav-bulk-transfer');
 *
 * webdavBulkTransfer.download('https://server.url', 'path/to/local/directory', ['/path/to/remote/directory','/path/to/remote/file.ext'], {
 *     username: 'username',
 *     password: 'password',
 *     recursive: true,
 *     concurrency: 10
 * });
 */
module.exports.download = (sourceURL, localdirectory, directoriesAndFiles, options) => main.download(sourceURL, localdirectory, directoriesAndFiles, options);

/**
 * Javascript interface to upload directories and files from a local directory to a webDAV server
 *
 * @alias module:index
 *
 * @param {String} localdirectory The local directory where the downloaded files will be stored before uploading them
 * @param {String} targetURL The target WebDAV URL
 * @param {Object} options Object containing all possible options
 *
 * @returns {Promise}
 * @resolve {Array} If everything goes well, returns the list of uploaded files
 * @reject {Error} Returns an error if one happened
 *
 * @example
 * // This command will upload files in the local directory recursively to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
 * const webdavBulkTransfer = require('webdav-bulk-transfer');
 *
 * webdavBulkTransfer.upload('path/to/local/directory', 'https://server.url', {
 *     username: 'username',
 *     password: 'password',
 *     recursive: true,
 *     concurrency: 10,
 *     cleanafter: true
 * });
 */
module.exports.upload = (localdirectory, targetURL, options) => main.upload(localdirectory, targetURL, options);

/**
 * Javascript interface to transfer directories and files from one webDAV server to another
 *
 * @alias module:index
 *
 * @param {String} sourceURL The source WebDAV URL
 * @param {String} targetURL The target WebDAV URL
 * @param {String} directoriesAndFiles A comma separated list of directories and files to transfer
 * @param {Object} options Object containing all possible options
 *
 * @returns {Promise}
 * @resolve {Array} If everything goes well, returns the list of transfered files
 * @reject {Error} Returns an error if one happened
 *
 * @example
 * // This command will transfer files recursively from the given source server to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
 * const webdavBulkTransfer = require('webdav-bulk-transfer');
 *
 * webdavBulkTransfer.transfer('https://source.server', 'https://targer.server', ['/path/to/remote/directory','/path/to/remote/file.ext'], {
 *     sourceusername: 'username',
 *     sourcepassword: 'password',
 *     targetusername: 'username',
 *     targetpassword: 'password',
 *     recursive: true,
 *     concurrency: 10,
 *     cleanafter: true,
 *     onlynewfiles: true,
 *     keeplocaldirectory: true
 * });
 */
module.exports.transfer = (sourceURL, targetURL, directoriesAndFiles, options) => main.transfer(sourceURL, targetURL, directoriesAndFiles, options);
