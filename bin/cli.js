#!/usr/bin/env node

'use strict';

/**
 * Command line interface for the WebDAV Bulk Transfer tool
 *
 * @example Download command
 * Usage: download <sourceURL> <localdirectory> <directoriesAndFiles>
 *
 * Options:
 * *    --username <value>    The username used to authenticate through the source.
 * *    --password <value>    The password used to authenticate through the source.
 * *    --quiet               Disable the verbose mode. Default: false
 * *    --concurrency <value> The number of calls that can be handled at one time by the tool. Default: 1
 * *    --recursive           Will transfer directories and files recursively (look in child directories if any). Default: true
 * *    --keeplocaldirectory  Will keep the local directory before performing the download action. This is useful when you already have some files and want to download the remaining files from the instance. Default: false
 *
 * The following examples will download directories and files recursively from the given server to the given local directory (10 concurrent connections will be opened each time)
 * // with node command
 * node bin/cli.js download "https://server.url" "path/to/local/directory" "/path/to/remote/directory,/path/to/remote/file.ext" --username "username" --password "password" --recursive --concurrency 10
 * // after a "npm link"
 * davtransfer download "https://server.url" "path/to/local/directory" "/path/to/remote/directory,/path/to/remote/file.ext" --username "username" --password "password" --recursive --concurrency 10
 *
 * @example Upload command
 * Usage: upload <localdirectory> <targetURL>
 *
 * Options:
 * *    --username <value>    The username used to authenticate through the source.
 * *    --password <value>    The password used to authenticate through the source.
 * *    --certificate <value> The path to the source certificate file.
 * *    --passphrase <value>  The passphrase to use with the given source certificate.
 * *    --quiet               Disable the verbose mode. Default: false
 * *    --cleanafter          Clean the local directory after the execution. Default: false
 * *    --concurrency <value> The number of calls that can be handled at one time by the tool. Default: 1
 * *    --recursive           Will transfer directories and files recursively (look in child directories if any). Default: true
 *
 * The following examples will upload files in the local directory recursively to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
 * // with node command
 * node bin/cli.js upload "path/to/local/directory" "https://server.url" --username "username" --password "password" --recursive --concurrency 10 --cleanafter
 * // after a "npm link"
 * davtransfer upload "path/to/local/directory" "https://server.url" --username "username" --password "password" --recursive --concurrency 10 --cleanafter
 *
 * @example Transfer command
 * Usage: transfer <sourceURL> <targetURL> <directoriesAndFiles>
 *
 * Options:
 * *    --localdirectory <value>    The local directory where the downloaded files will be stored before uploading them. Default: "downloaded_files"
 * *    --sourceusername <value>    The username used to authenticate through the source.
 * *    --sourcepassword <value>    The password used to authenticate through the source.
 * *    --sourcecertificate <value> The path to the source certificate file.
 * *    --sourcepassphrase <value>  The passphrase to use with the given source certificate.
 * *    --targetusername <value>    The username used to authenticate through the target.
 * *    --targetpassword <value>    The password used to authenticate through the target.
 * *    --targetcertificate <value> The path to the target certificate file.
 * *    --targetpassphrase <value>  The passphrase to use with the given target certificate.
 * *    --concurrency <value>       The number of calls that can be handled at one time by the tool. Default: 1
 * *    --quiet                     Disable the verbose mode. Default: false
 * *    --cleanafter                Clean the local temp directory after the execution. Default: false
 * *    --recursive                 Will transfer directories and files recursively (look in child directories if any). Default: true
 * *    --onlynewfiles              Will only transfer files from the source that does not exists in the target. Default: false
 * *    --keeplocaldirectory        Will keep the local directory before performing the transfer action. This is useful when you already have some files to transfer locally, and only want to download the remaining files before uploading everything. Default: false
 *
 * The following examples will transfer files recursively from the given source server to the given server (10 concurrent connections will be opened each time). Then the local directory will be removed.
 * // with node command
 * node bin/cli.js transfer "https://source.server" "https://targer.server" --sourceusername "username" --sourcepassword "password" --targetusername "username" --targerpassword "password" --recursive --concurrency 10 --cleanafter
 * // after a "npm link"
 * davtransfer transfer "https://source.server" "https://targer.server" --sourceusername "username" --sourcepassword "password" --targetusername "username" --targerpassword "password" --recursive --concurrency 10 --cleanafter
 *
 * @module cli
 */

/**
 * @type {Object}
 */
const packageDetails = require('../package');
/**
 * @type {node_modules/commander}
 */
const program = require('commander');

/**
 * @type {lib/log}
 */
const log = require('../lib/log');
/**
 * @type {src/main}
 */
const main = require('../src/main');

process.title = packageDetails.name;
log.info(packageDetails.name, packageDetails.version);
log.info('');

program.version(packageDetails.version).description(packageDetails.description);

/**
 * Command line interface to download directories and files from a webDAV server to a local directory
 *
 * @alias module:cli
 *
 * @param {String} download The command name
 * @param {String} sourceURL The source WebDAV URL
 * @param {String} localdirectory The local directory where the downloaded files will be stored before uploading them
 * @param {String} directoriesAndFiles A comma separated list of directories and files to download
 */
program.command('download <sourceURL> <localdirectory> <directoriesAndFiles>')
    .option('--username <value>', 'The username used to authenticate through the source.')
    .option('--password <value>', 'The password used to authenticate through the source.')
    .option('--quiet', 'Disable the verbose mode. Default: false')
    .option('--concurrency <value>', 'The number of calls that can be handled at one time by the tool. Default: 1')
    .option('--recursive', 'Will download directories and files recursively (look in child directories if any). Default: true')
    .option('--keeplocaldirectory', 'Will keep the local directory folder so that it won\'t download from scratch. Default: false')
    .action((sourceURL, localdirectory, directoriesAndFiles, options) => {
        Promise.resolve().then(() => main.download(sourceURL, localdirectory, directoriesAndFiles, options))
            .then(() => process.exit(0))
            .catch(e => {
                log.error(e);
                process.exit(-1);
            });
    });

/**
 * Command line interface to upload directories and files from a local directory to a webDAV server
 *
 * @alias module:cli
 *
 * @param {String} upload The command name
 * @param {String} localdirectory The local directory where the downloaded files will be stored before uploading them
 * @param {String} targetURL The target WebDAV URL
 */
program.command('upload <localdirectory> <targetURL>')
    .option('--username <value>', 'The username used to authenticate through the source.')
    .option('--password <value>', 'The password used to authenticate through the source.')
    .option('--certificate <value>', 'The path to the source certificate file.')
    .option('--passphrase <value>', 'The passphrase to use with the given source certificate.')
    .option('--quiet', 'Disable the verbose mode. Default: false')
    .option('--concurrency <value>', 'The number of calls that can be handled at one time by the tool. Default: 1')
    .option('--cleanafter', 'Clean the local directory after the execution. Default: false')
    .option('--recursive', 'Will upload directories and files recursively (look in child directories if any). Default: true')
    .option('--onlynewfiles', 'Will only upload files from the source that does not exists in the target. Default: false')
    .action((localdirectory, targetURL, options) => {
        Promise.resolve().then(() => main.upload(localdirectory, targetURL, options))
            .then(() => process.exit(0))
            .catch(e => {
                log.error(e);
                process.exit(-1);
            });
    });

/**
 * Command line interface to transfer directories and files from one webDAV server to another
 *
 * @alias module:cli
 *
 * @param {String} transfer The command name
 * @param {String} sourceURL The source WebDAV URL
 * @param {String} targetURL The target WebDAV URL
 * @param {String} directoriesAndFiles A comma separated list of directories and files to transfer
 */
program.command('transfer <sourceURL> <targetURL> <directoriesAndFiles>')
    .option('--localdirectory <value>', 'The local directory where the downloaded files will be stored before uploading them. Default: "downloaded_files"')
    .option('--sourceusername <value>', 'The username used to authenticate through the source.')
    .option('--sourcepassword <value>', 'The password used to authenticate through the source.')
    .option('--sourcecertificate <value>', 'The path to the source certificate file.')
    .option('--sourcepassphrase <value>', 'The passphrase to use with the given source certificate.')
    .option('--targetusername <value>', 'The username used to authenticate through the target.')
    .option('--targetpassword <value>', 'The password used to authenticate through the target.')
    .option('--targetcertificate <value>', 'The path to the target certificate file.')
    .option('--targetpassphrase <value>', 'The passphrase to use with the given target certificate.')
    .option('--concurrency <value>', 'The number of calls that can be handled at one time by the tool. Default: 1')
    .option('--quiet', 'Disable the verbose mode. Default: false')
    .option('--cleanafter', 'Clean the local temp directory after the execution. Default: false')
    .option('--recursive', 'Will transfer directories and files recursively (look in child directories if any). Default: true')
    .option('--onlynewfiles', 'Will only transfer files from the source that does not exists in the target. Default: false')
    .option('--keeplocaldirectory', 'Will keep the local directory folder so that it won\'t download from scratch. Default: false')
    .action((sourceURL, targetURL, directoriesAndFiles, options) => {
        Promise.resolve().then(() => main.transfer(sourceURL, targetURL, directoriesAndFiles, options))
            .then(() => process.exit(0))
            .catch(e => {
                log.error(e);
                process.exit(-1);
            });
    });

// parse CLI arguments
program.parse(process.argv);

// output help message if no arguments provided
if (!process.argv.slice(2).length) {
    program.help();
}
