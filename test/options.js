'use strict';

/**
 * @type {node_modules/chai.expect}
 */
const expect = require('chai').expect;
/**
 * @type {node_modules/path}
 */
const path = require('path');

/**
 * @type {lib/options}
 */
const options = require('../lib/options');

/**
 * @type Object
 */
const OPTS = {
    DOWNLOAD: {
        username: 'username',
        password: 'password',
        recursive: true,
        concurrency: 10
    },
    UPLOAD: {
        username: 'username',
        password: 'password',
        recursive: true,
        concurrency: 10,
        cleanafter: true,
        onlynewfiles: true
    },
    TRANSFER: {
        localdirectory: 'downloaded_files/to/fake/local/dir',
        sourceusername: 'username',
        sourcepassword: 'password',
        targetusername: 'username',
        targetpassword: 'password',
        recursive: true,
        concurrency: 10,
        cleanafter: true,
        onlynewfiles: true
    }
};

/**
 * @type Object
 */
const EXPECTED = {
    DEFAULT: {
        DOWNLOAD: {
            concurrency: 1,
            sourceCredentials: {},
            localDirectory: path.resolve(process.cwd(), 'downloaded_files')
        },
        UPLOAD: {
            concurrency: 1,
            sourceCredentials: {},
            targetCredentials: {},
            localDirectory: path.resolve(process.cwd(), 'downloaded_files')
        },
        TRANSFER: {
            concurrency: 1,
            localDirectory: path.resolve(process.cwd(), 'downloaded_files'),
            sourceCredentials: {},
            targetCredentials: {}
        }
    },
    WITHINPUTS: {
        DOWNLOAD: {
            sourceCredentials: {
                username: OPTS.DOWNLOAD.username,
                password: OPTS.DOWNLOAD.password,
                certificate: undefined,
                passphrase: undefined
            },
            concurrency: OPTS.DOWNLOAD.concurrency,
            directoriesAndFilesToDownload: [
                'downloaded_files/to/folder',
                'downloaded_files/to/file.txt'
            ],
            cleanAfter: false,
            onlyNewFiles: false,
            quiet: false,
            recursive: OPTS.DOWNLOAD.recursive,
            keepLocalDirectory: false,
            targetCredentials: {},
            localDirectory: path.resolve(process.cwd(), 'downloaded_files')
        },
        UPLOAD: {
            targetCredentials: {
                username: OPTS.UPLOAD.username,
                password: OPTS.UPLOAD.password,
                certificate: undefined,
                passphrase: undefined
            },
            concurrency: OPTS.UPLOAD.concurrency,
            directoriesAndFilesToDownload: [
                'downloaded_files/to/folder',
                'downloaded_files/to/file.txt'
            ],
            cleanAfter: OPTS.UPLOAD.cleanafter,
            onlyNewFiles: OPTS.UPLOAD.onlynewfiles,
            quiet: false,
            recursive: OPTS.UPLOAD.recursive,
            sourceCredentials: {},
            localDirectory: path.resolve(process.cwd(), 'downloaded_files'),
            keepLocalDirectory: false
        },
        TRANSFER: {
            concurrency: OPTS.TRANSFER.concurrency,
            directoriesAndFilesToDownload: [
                'downloaded_files/to/folder',
                'downloaded_files/to/file.txt'
            ],
            cleanAfter: OPTS.TRANSFER.cleanafter,
            onlyNewFiles: OPTS.TRANSFER.onlynewfiles,
            keepLocalDirectory: false,
            quiet: false,
            recursive: OPTS.TRANSFER.recursive,
            sourceCredentials: {
                username: OPTS.TRANSFER.sourceusername,
                password: OPTS.TRANSFER.sourcepassword,
                certificate: undefined,
                passphrase: undefined
            },
            targetCredentials: {
                username: OPTS.TRANSFER.targetusername,
                password: OPTS.TRANSFER.targetpassword,
                certificate: undefined,
                passphrase: undefined
            },
            localDirectory: path.resolve(process.cwd(), 'downloaded_files/to/fake/local/dir')
        }
    }
};

describe('lib/options.js', () => {
    describe('Options parsing', () => {
        it('Parse default options', () => {
            var opts = options.parseDownloadOptions(undefined);
            expect(opts).to.deep.equal(EXPECTED.DEFAULT.DOWNLOAD);

            opts = options.parseUploadOptions(undefined);
            expect(opts).to.deep.equal(EXPECTED.DEFAULT.UPLOAD);

            opts = options.parseTransferOptions(undefined);
            expect(opts).to.deep.equal(EXPECTED.DEFAULT.TRANSFER);
        });

        it('Parse and throw errors', () => {
            process.env.quiet = true;
            process.env.runningAsScript = true;

            var opts = Object.assign({}, OPTS.UPLOAD);
            let p = 'downloaded_files/to/fake/certificate/file.p12';
            opts.certificate = p;

            try {
                options.parseDownloadOptions(opts);
            } catch (e) {
                expect(e.message).to.equal(`The given certificate file does not exists at path "${p}".`);
            }

            p = './lib';
            opts.certificate = p;
            try {
                options.parseDownloadOptions(opts);
            } catch (e) {
                expect(e.message).to.equal(`The given certificate is not a file at path "${p}".`);
            }

            try {
                // fake but valid certificate path
                opts.certificate = '.gitignore';
                options.parseDownloadOptions(opts, 'downloaded_files', '');
            } catch (e) {
                expect(e.message).to.equal('No directories or files to transfer. Abort.');
            }
        });

        it('Parse Download options', () => {
            var opts = options.parseDownloadOptions(OPTS.DOWNLOAD, 'downloaded_files', 'downloaded_files/to/folder,downloaded_files/to/file.txt');
            expect(opts).to.deep.equal(EXPECTED.WITHINPUTS.DOWNLOAD);
        });

        it('Parse Upload options', () => {
            var opts = options.parseUploadOptions(OPTS.UPLOAD, 'downloaded_files', 'downloaded_files/to/folder,downloaded_files/to/file.txt');
            expect(opts).to.deep.equal(EXPECTED.WITHINPUTS.UPLOAD);
        });

        it('Parse Transfer options', () => {
            var opts = options.parseTransferOptions(OPTS.TRANSFER, 'downloaded_files/to/folder,downloaded_files/to/file.txt');
            expect(opts).to.deep.equal(EXPECTED.WITHINPUTS.TRANSFER);
        });
    });
});
