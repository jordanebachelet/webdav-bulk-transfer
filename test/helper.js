'use strict';

/**
 * @type {node_modules/chai.expect}
 */
const expect = require('chai').expect;

/**
 * @type {lib/helper}
 */
const helper = require('../lib/helper');

describe('lib/helper.js', () => {
    describe('Helper methods', () => {
        it('test mkdirRecursive', () => {
            process.env.quiet = true;
            process.env.runningAsScript = undefined;

            expect(helper.mkdirRecursive('long/path/to/something')).to.equal(true);
        });

        it('test cleanDirectory error', () => {
            process.env.quiet = true;
            process.env.runningAsScript = true;

            let p = 'test/to/directory/that/does/not/exists';

            return helper.cleanDirectory(p).then(res => {
                throw new Error(res);
            }, res => {
                expect(res.message).to.deep.equal(`The directory "${p}" does not exists. Abort.`);
            });
        });

        it('test cleanDirectory success', () => {
            process.env.quiet = true;
            process.env.runningAsScript = true;

            return helper.cleanDirectory('./long').then(res => {
                expect(res).to.equal(undefined);
            });
        });


        it('test getFilesRecursively to the ./ folder', () => {
            process.env.quiet = undefined;
            process.env.runningAsScript = true;

            let res = helper.getFilesRecursively();
            expect(res).to.be.an('array');
            expect(res.length).to.equal(0);

            res = helper.getFilesRecursively('test-results', 'test-results', true);
            expect(res).to.be.an('array');
            expect(res.length).to.not.equal(0);
        });
    });
});
