'use strict';

/**
 * @type {node_modules/chai.expect}
 */
const expect = require('chai').expect;

/**
 * @type {lib/log}
 */
const log = require('../lib/log');

describe('lib/log.js', () => {
    describe('Custom logger methods', () => {
        it('test quiet mode', () => {
            process.env.quiet = true;
            process.env.runningAsScript = undefined;

            expect(log.isQuiet(true)).to.equal(false);
            expect(log.isQuiet()).to.equal(true);

            process.env.quiet = undefined;
            process.env.runningAsScript = true;

            expect(log.isQuiet(true)).to.equal(true);
            expect(log.isQuiet()).to.equal(true);

            process.env.quiet = true;
            process.env.runningAsScript = true;

            expect(log.isQuiet(true)).to.equal(true);
            expect(log.isQuiet()).to.equal(true);
        });

        it('test with quiet mode', () => {
            process.env.quiet = true;
            process.env.runningAsScript = true;

            expect(log.info('info logged')).to.equal(undefined);
            expect(log.warn('warn logged')).to.equal(undefined);
            expect(log.success('success logged')).to.equal(undefined);
            expect(log.error('error logged')).to.equal(undefined);
        });


        it('test without quiet mode but running as script', () => {
            process.env.quiet = undefined;
            process.env.runningAsScript = true;

            expect(log.info('info logged')).to.equal(undefined);
            expect(log.warn('warn logged')).to.equal(undefined);
            expect(log.success('success logged')).to.equal(undefined);
            expect(log.error('error logged')).to.equal(undefined);
        });

        it('test without quiet mode', () => {
            process.env.quiet = undefined;
            process.env.runningAsScript = undefined;

            expect(log.info('info logged')).to.equal(true);
            expect(log.warn('warn logged')).to.equal(true);
            expect(log.success('success logged')).to.equal(true);
            expect(log.error('error logged')).to.equal(true);
        });
    });
});
